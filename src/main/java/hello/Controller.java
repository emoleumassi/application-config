/*
 * Copyright 2018 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */
package hello;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import hello.bootstrap.ApplicationConfig;
import hello.bootstrap.Database;
import hello.bootstrap.Role;

/**
 * @author emo.leumassi
 */
@RestController
public class Controller {

	@Autowired
	private ApplicationConfig applicationConfig;

	@ResponseBody
	@GetMapping(value = "/", produces = "application/json")
	public ResponseEntity<ApplicationConfig> getAll() {
		return new ResponseEntity<>(applicationConfig, HttpStatus.OK);
	}

	@ResponseBody
	@GetMapping(value = "/database", produces = "application/json")
	public ResponseEntity<List<Database>> getDatabase() {
		return new ResponseEntity<>(applicationConfig.getDatabase(), HttpStatus.OK);
	}

	@ResponseBody
	@GetMapping(value = "/role", produces = "application/json")
	public ResponseEntity<Role> getRole() {
		return new ResponseEntity<>(applicationConfig.getRole(), HttpStatus.OK);
	}
}
