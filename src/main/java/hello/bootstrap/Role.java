/*
 * Copyright 2018 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */
package hello.bootstrap;

import java.util.List;

/**
 * @author emo.leumassi
 */
public class Role {

	private List<String> admin;
	private List<String> editor;

	public List<String> getAdmin() {
		return admin;
	}

	public void setAdmin(final List<String> admin) {
		this.admin = admin;
	}

	public List<String> getEditor() {
		return editor;
	}

	public void setEditor(final List<String> editor) {
		this.editor = editor;
	}
}
