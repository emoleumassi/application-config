/*
 * Copyright 2018 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */
package hello.bootstrap;

/**
 * @author emo.leumassi
 */
public class Password {

	private String dba;
	private String read;
	private String write;

	public String getDba() {
		return dba;
	}

	public void setDba(final String dba) {
		this.dba = dba;
	}

	public String getRead() {
		return read;
	}

	public void setRead(final String read) {
		this.read = read;
	}

	public String getWrite() {
		return write;
	}

	public void setWrite(final String write) {
		this.write = write;
	}
}
