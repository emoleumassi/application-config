/*
 * Copyright 2018 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */
package hello.bootstrap;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author emo.leumassi
 */
@Component
@ConfigurationProperties("swaw")
public class ApplicationConfig {

	private String stage;
	private String ip;
	private Role role;
	private List<Database> database;

	public String getStage() {
		return stage;
	}

	public void setStage(final String stage) {
		this.stage = stage;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(final String ip) {
		this.ip = ip;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(final Role role) {
		this.role = role;
	}

	public List<Database> getDatabase() {
		return database;
	}

	public void setDatabase(final List<Database> database) {
		this.database = database;
	}
}
