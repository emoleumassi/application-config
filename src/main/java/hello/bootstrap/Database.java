/*
 * Copyright 2018 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */
package hello.bootstrap;

import java.util.List;

/**
 * @author emo.leumassi
 */
public class Database {

	private String url;
	private String name;
	private String host;
	private String port;
	private String driver;
	private String defaultSchemaName;
	private List<User> user;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(final String port) {
		this.port = port;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(final String driver) {
		this.driver = driver;
	}

	public String getDefaultSchemaName() {
		return defaultSchemaName;
	}

	public void setDefaultSchemaName(final String defaultSchemaName) {
		this.defaultSchemaName = defaultSchemaName;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(final List<User> user) {
		this.user = user;
	}
}
